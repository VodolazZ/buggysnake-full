﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;
using System.Text;

namespace Assets.Scripts.UI
{
    class PauseWindowScript : MonoBehaviour
    {
        public GameObject ContinueButton;
        public GameObject QuitButton;

        private EventSystem sceneEventSys;
        private OnlyKeyBoardInputModule sceneInputModule;

        // Use this for initialization
        void Awake()
        {
            sceneEventSys = FindObjectOfType<EventSystem>();
            sceneInputModule = sceneEventSys.gameObject.GetComponent<OnlyKeyBoardInputModule>();
        }

        void Start()
        {
            OnEnable();
        }

        void OnEnable()
        {
            SetActiveField(ContinueButton);
        }

        private void SetActiveField(GameObject target)
        {
            sceneEventSys.SetSelectedGameObject(null);
            sceneEventSys.SetSelectedGameObject(target);
        }


        public void ContinueButtonPressed()
        {
            Facade.UnPause();
        }

        public void MainMenuButtonPressed()
        {
            Facade.UnPause();
        }
    }
}
