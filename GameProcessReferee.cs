﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Project.Scores;


namespace SnakeBoxer.Referees
{
    public class GameProcessReferee : MonoBehaviour
    {
        public  bool isPaused = false;
        private float waitingTimeOnSceneLoad = 1f;
        private const float RELOADING_STEP_SPEED = 0.05f;
        private const float RELOADING_GAMEOVER_STEP_SPEED = 0.07f;

        private const float MIN_SNAKE_SPEED = 0.3f;
        private const float INCREASE_SPEED_PERLVL = 0.04f;
        private const int MAX_LVL = 9;
        private int currentLevel;
        public int CurrentLevel
        {
            get { return currentLevel; }
            private set
            {
                if (value > 0 && value <= MAX_LVL)
                {                   
                    currentLevel = value;
                    Debug.Log(INCREASE_SPEED_PERLVL - ((float)currentLevel / 1000));
                    CurrentSnakeSpeed = MIN_SNAKE_SPEED - (INCREASE_SPEED_PERLVL-((float)currentLevel/1000*1.5f))*currentLevel;
                    Debug.Log("new level: " + currentLevel + " new speed: " + currentSnakeSpeed);                  
                }
            }
        }
        private float currentSnakeSpeed;
        private float CurrentSnakeSpeed
        {
            get { return currentSnakeSpeed; }
            set { if (value > 0 && value < MIN_SNAKE_SPEED) currentSnakeSpeed = value; }
        }

        private uint score;
        public uint Score
        {
            get { return (uint)score; }
            set { if (value >= 0 && score < uint.MaxValue) score = (uint)value; }
        }
        private int currentBorder;

        public ScoresHolder myScoreBoard;
        public GameObject UIPanel;
        public GameObject UIPanelScoreBlock;
        public GameObject UIPanelPauseBlock;
        public Text headingText;

        public Directions lastChoosenDirection = Directions.Right;
        public Directions lastStepDirection;

        private const byte MAX_NUM_OF_LIVES = 2;
        private byte currentNumOfLives;
        public byte CurrentNumOfLives
        {
            get { return currentNumOfLives; }
            set { if (value >= 0 && value <= MAX_NUM_OF_LIVES) currentNumOfLives = value; }
        }
        
        public static int countMe = 0;
        private static GameProcessReferee singleton;

        private void TurnOnHighScoresPanel()
        {
            headingText.text = "New highscore!";            
            UIPanel.SetActive(true);
            UIPanelScoreBlock.SetActive(true);            
        }

        public void TurnOnPausePanel()
        {
            CancelInvoke();
            isPaused = true;
            headingText.text = "Pause";
            UIPanel.SetActive(true);
            UIPanelPauseBlock.SetActive(true);
        }

        public void TurnOffPausePanel()
        {
            UIPanel.SetActive(false);
            UIPanelPauseBlock.SetActive(false);
            isPaused = false;
            InvokeRepeating("InitiateNextStep", 0f, CurrentSnakeSpeed);
        }

        void Awake()
        {
            if (singleton == null) singleton = this;
            else { Destroy(this); return; }
            Facade.CurrentGameReferee = this;

            CurrentNumOfLives = MAX_NUM_OF_LIVES;
            Debug.Log(CurrentNumOfLives);
            CurrentLevel = 1;
            isPaused = false;                   
        }

        void Start()
        {
            currentBorder = Facade.CurrentGameSettings.borderCollections[currentLevel];
            myScoreBoard = new ScoresHolder();

            Debug.Log("scoreboard count: " + myScoreBoard.GetActualRecordList().Count);
        }

        public void InitializationComplete()
        {
            InvokeRepeating("InitiateNextStep", waitingTimeOnSceneLoad, CurrentSnakeSpeed);
        }

        //Events
        public static event Action nextStepHasBeenMade;
        public static event Action LevelReloadComplete;
        public static event Action GameOverConditionsWereMet;
        public static event Action GameOverReloadCompleted;
        public static event Action DefeatConditions;
        public static event Action<int> ChangeLevelEvent;
        public static event Action<uint> ChangeScoreEvent;
        public static event Action PauseEvent;

        //Event shooters
        private void InitiateNextStep()
        {
            nextStepHasBeenMade();
        }

        public void LevelReloadCompleted()
        {
            CancelInvoke();
            LevelReloadComplete();
            lastChoosenDirection = Directions.Right;
            InvokeRepeating("InitiateNextStep", waitingTimeOnSceneLoad, currentSnakeSpeed);
        }
               
        public void GamePauser()
        { }

        public void DefeatConditionsFulfilled()
        {
            CancelInvoke();
            DefeatConditions();
            Facade.SnakeLength = Facade.initSnakeTailLength;
            InvokeRepeating("InitiateNextStep", 0f, RELOADING_STEP_SPEED);
        }

        public void ScoreChanged(int value)
        {
            Score += (uint)value;
            if (currentLevel < MAX_LVL && Score > currentBorder)
            {
                CurrentLevel++;
                currentBorder = Facade.CurrentGameSettings.borderCollections[currentLevel];
                ChangeScoreEvent(Score);
                GraduateToNextLevel();
            }
            if (ChangeScoreEvent != null && ChangeScoreEvent.GetInvocationList().Length > 0) ChangeScoreEvent(Score);
        }

        private void GraduateToNextLevel()
        {
            if (ChangeLevelEvent != null && ChangeLevelEvent.GetInvocationList().Length > 0) ChangeLevelEvent(CurrentLevel);
            CancelInvoke();
            InvokeRepeating("InitiateNextStep", currentSnakeSpeed, currentSnakeSpeed);
        }

        public void InitiateGameOverConditionsWereMet()
        {
            CancelInvoke();
            GameOverConditionsWereMet();
            Facade.SnakeLength = Facade.initSnakeTailLength;
            InvokeRepeating("InitiateNextStep", 0f, RELOADING_GAMEOVER_STEP_SPEED);
        }

        public void GameOverReloadScreenFinished()
        {
            CancelInvoke();
            if (Facade.CheckForRecord(Facade.GetPlayerScore()))
            {
                TurnOnHighScoresPanel();
            }
            else CloseGameScene();
        }

        public void CloseGameScene()
        {
            if (GameOverReloadCompleted != null && GameOverReloadCompleted.GetInvocationList().Length > 0) GameOverReloadCompleted();
            SceneManager.LoadScene("HighscoreScene", LoadSceneMode.Single);
            Destroy(this);
        }
    }
}


