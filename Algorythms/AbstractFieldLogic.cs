﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class AbstractFieldLogic
{
    public virtual void Draw(ref SpriteRenderer initiator, Color newColor)
    {
        initiator.color = newColor;
    }

    public abstract Color CalculateMyColor(int TTL);
}