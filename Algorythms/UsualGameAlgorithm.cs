﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class UsualGameAlgorithm : AbstractLevelAlgorithm
{
    private static int baseReward = 15;

    public override void GameStepHandler(FieldElement[,] gameField, ref DecartCoords currHeadPos, Stack<FieldElement> changedPositns)
    {
        int rewardWasTaken = 0;

        DecartCoords nextSnakeHeadPosition = new DecartCoords(currHeadPos.xCoord, currHeadPos.yCoord); 
        switch (Facade.LastChoosenDirection)
        {
            case Directions.Up: nextSnakeHeadPosition.yCoord++; Facade.lastStepDirection = Directions.Up; break;
            case Directions.Right: nextSnakeHeadPosition.xCoord++; Facade.lastStepDirection = Directions.Right; break;
            case Directions.Down: nextSnakeHeadPosition.yCoord--; Facade.lastStepDirection = Directions.Down; break;
            case Directions.Left: nextSnakeHeadPosition.xCoord--; Facade.lastStepDirection = Directions.Left;  break;
            default: break;
        } //where snake head should be on the next step

        if (ObstacleChecker(gameField, ref nextSnakeHeadPosition)) //check new place for obstacles, finish game if true
        {
            if (--Facade.NumberOfLives <= 0) Facade.GameOverConditionsWereMet();            
            else Facade.InitiateDeath();            
            return;
        }

        switch (CollectableChecker(gameField, nextSnakeHeadPosition))
        {
            case FieldElementState.Reward:
                FoodInstantiator(gameField, changedPositns);
                rewardWasTaken = 1;
                Facade.SnakeLength += rewardWasTaken;
                Facade.ScoreChanged(baseReward * Facade.CurrentLevel);
                break;
            default: break;
        }

        for (int i = 0; i < Facade.fieldWidth; i++)
            for (int k = 0; k < Facade.fieldHeight; k++)
            {
                if (gameField[i, k].MyCurrentState == FieldElementState.Body)
                {
                    gameField[i, k].TTL += rewardWasTaken;
                    if (gameField[i, k].TTL <= 0) { gameField[i, k].SwitchField(FieldElementState.Empty); gameField[i, k].TTL = int.MaxValue; }
                    else { gameField[i, k].TTL--; gameField[i, k].RecalculateMyColor(); }
                    changedPositns.Push(gameField[i, k]);
                }
            }
        gameField[nextSnakeHeadPosition.xCoord, nextSnakeHeadPosition.yCoord].SwitchField(FieldElementState.Head);
        changedPositns.Push(gameField[nextSnakeHeadPosition.xCoord, nextSnakeHeadPosition.yCoord]);
        gameField[currHeadPos.xCoord, currHeadPos.yCoord].SwitchField(FieldElementState.Body);
        changedPositns.Push(gameField[currHeadPos.xCoord, currHeadPos.yCoord]);
        gameField[currHeadPos.xCoord, currHeadPos.yCoord].TTL = Facade.SnakeLength - 1;        
        currHeadPos = nextSnakeHeadPosition;
    }

    public override bool ObstacleChecker(FieldElement[,] gameField, ref DecartCoords newHeadPos)
    {
        if (newHeadPos.xCoord < 0 || newHeadPos.xCoord >= Facade.fieldWidth || newHeadPos.yCoord < 0 || newHeadPos.yCoord >= Facade.fieldHeight)
            return true;
        else if (gameField[newHeadPos.xCoord, newHeadPos.yCoord].MyCurrentState == FieldElementState.Body ||
            gameField[newHeadPos.xCoord, newHeadPos.yCoord].MyCurrentState == FieldElementState.Wall ||
            gameField[newHeadPos.xCoord, newHeadPos.yCoord].MyCurrentState == FieldElementState.Head)
            return true;

        else return false;
    }

    public override void StartObjectInitializator(FieldElement[,] gameField, Stack<FieldElement> changedPositions)
    {
        base.StartObjectInitializator(gameField, changedPositions);
        FoodInstantiator(gameField, changedPositions);
    }

    private void FoodInstantiator(FieldElement[,] gameField, Stack<FieldElement> changedPositions)
    {
        System.Random foodRndmzr = new System.Random();
        while (true)
        {
            int x = foodRndmzr.Next(Facade.fieldWidth);
            int y = foodRndmzr.Next(Facade.fieldHeight);
            DecartCoords dc = new DecartCoords(x, y);
            if (!ObstacleChecker(gameField, ref dc))
            {
                gameField[x, y].SwitchField(FieldElementState.Reward);
                changedPositions.Push(gameField[x, y]);
                break;
            }
        }
    }
}