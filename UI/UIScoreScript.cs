﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeBoxer.UI
{
    class UIScoreScript : MonoBehaviour
    {
        private const string baseText = "Score: ";
        private Text scoreText;

        void Awake()
        {
            scoreText = GetComponent<Text>();
        }
        void Start()
        {
            Facade.RegisterForScoreChange(ScoreChangeHandler);
            Facade.RegisterForGameOver(GameOverHandler);
            ScoreChangeHandler(0);
        }

        public void ScoreChangeHandler(uint newScoreValue)
        {
            scoreText.text = baseText + newScoreValue;
        }

        public void GameOverHandler()
        {
            Facade.UnRegisterForScoreChange(ScoreChangeHandler);
            Facade.UnRegisterForGameOver(GameOverHandler);
            Destroy(this);
        }
    }
}
