# BuggySnake #

Репозиторий содержит исходный код змейки BuggySnake.

### Про BuggySnake ###

Программа используется как тестовое задание для стажеров в группу тестирования. 
Основная проблема, которую решает программа - массовая доступность и известность утилиты ListBoxxer, позволяющая подготовиться к интервью на позицию тест-специалиста с использованием этой утилиты. Данной же аркады в свободном доступе нет и достать ее не получится.
Приложение разворачивается копипастом.


### Как это выглядит ###

Игра содержит несколько игровых режимов для более интересного процесса тестирования. Сейвы сбрасываются в папку, где развернуто приложение.

Классический режим. Стены-препятствия, за рамки поля выйти нельзя.

![446c29235b5d56f7a6765417602cb31a.png](https://bitbucket.org/repo/RGEogr/images/1234288735-446c29235b5d56f7a6765417602cb31a.png)

Зеркальные рамки поля. Сетка поля отключена. Количество стен увеличено.

![8d618e8b719c505dad159cec531c3102.png](https://bitbucket.org/repo/RGEogr/images/1363577367-8d618e8b719c505dad159cec531c3102.png)

Гибкость настроек игрового процесса достигается в основном за счет динамической смены игрового алгоритма.