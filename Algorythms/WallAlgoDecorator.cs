﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class WallAlgoDecorator : AbstractAlgoDecorator
{
    public override void StartObjectInitializator(FieldElement[,] gameField, Stack<FieldElement> changedPositions)
    {
        base.StartObjectInitializator(gameField, changedPositions);

        int wallsAlreadyPlaced = 0;
        int numberOfWallsToBePlaced = Facade.numberOfWallsToBePlaced;
        Random wallRnd = new Random();
        
        while (wallsAlreadyPlaced < numberOfWallsToBePlaced)
        {
            int x = wallRnd.Next(0, Facade.fieldWidth-1);
            int y = wallRnd.Next(0, Facade.fieldHeight-1);

            if (TryToPlaceWall(gameField, new DecartCoords(x, y)))
            {
                changedPositions.Push(gameField[x, y]);
                wallsAlreadyPlaced++;
            }                                                       
        }       
    }    

    private bool TryToPlaceWall(FieldElement[,] gameField, DecartCoords place)
    {
        if (!decoratedAlgo.ObstacleChecker(gameField, ref place) && decoratedAlgo.CollectableChecker(gameField, place) == FieldElementState.Empty) //do not place wall immideately after snake's head in direction of movement
        { //now we check there is no obstacles around the place to protect algorithm from creating "death pockets"
            if ((place.xCoord == 0 || place.yCoord == 0 || gameField[place.xCoord - 1, place.yCoord - 1].MyCurrentState == FieldElementState.Empty) &&
                (place.xCoord == 0 || place.yCoord == Facade.fieldHeight - 1 || gameField[place.xCoord - 1, place.yCoord + 1].MyCurrentState == FieldElementState.Empty) &&
                (place.xCoord == Facade.fieldWidth - 1 || place.yCoord == 0 || gameField[place.xCoord + 1, place.yCoord - 1].MyCurrentState == FieldElementState.Empty) &&
                (place.xCoord == Facade.fieldWidth - 1 || place.yCoord == Facade.fieldHeight - 1 || gameField[place.xCoord + 1, place.yCoord + 1].MyCurrentState == FieldElementState.Empty))
            {
                //IMPLEMENTED BUG в общем случае этот алгоритм ставит стены достаточно грамотно, оставляя место для размещаемых предметов и прохода змейки. Но когда размещение пищи выпадает случайным образом на угловую клетку, а стены - рядом на горизонтальной или вертикальной границе игрового поля, это приводит к патовой ситуации, т.к. поднять пищу, не погибнув, невозможно.
                //IMPLEMENTED BUG алгоритм может поставить стену в угол игрового поля, что само по себе достаточно бесполезно с точки зрения усложнения геймплея.
                //IMPLEMENTED BUG алгоритм может поставить стену прямо перед "лицом" змейки, что заставит игрока на старте быстро давить клавишу управления
                gameField[place.xCoord, place.yCoord].SwitchField(FieldElementState.Wall);
                return true;
            }                
        }
        return false;
    }
}
