﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class GameOverReloadFieldAlgorithm : AbstractLevelAlgorithm
{
    public override void GameStepHandler(FieldElement[,] gameField, ref DecartCoords currHeadPos, Stack<FieldElement> changedPositions)
    {
        int colouredRow = 0;
        bool colouringComplete = false;

        for (colouredRow = 0; colouredRow < Facade.fieldHeight; colouredRow++)
        {
            if (gameField[1, colouredRow].MyCurrentState != FieldElementState.Coloured)
            {
                for (int k = 0; k < Facade.fieldWidth; k++)
                {
                    gameField[k, colouredRow].SwitchField(FieldElementState.Coloured);
                    gameField[k, colouredRow].TTL = int.MaxValue;
                    changedPositions.Push(gameField[k, colouredRow]);
                }
                colouringComplete = true;
                break;
            }
        }

        if (!colouringComplete)
        {
            Facade.GameOverFulfilled();
        }
    }

    public override bool ObstacleChecker(FieldElement[,] gameField, ref DecartCoords newHeadPos)
    {
        throw new NotImplementedException();
    }
}
