﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum FieldElementState
{
    Empty,
    Body,
    Head,
    Reward,
    Wall,
    Coloured
}