﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class AbstractLevelAlgorithm
{
    public abstract void GameStepHandler(FieldElement[,] gameField, ref DecartCoords currHeadPos, Stack<FieldElement> changedPositions);
    public virtual void StartObjectInitializator(FieldElement[,] gameField, Stack<FieldElement> changedPositions)
    {
        gameField[Facade.initialSnakeHeadPosition.xCoord, Facade.initialSnakeHeadPosition.yCoord].SwitchField(FieldElementState.Head);
        changedPositions.Push(gameField[Facade.initialSnakeHeadPosition.xCoord, Facade.initialSnakeHeadPosition.yCoord]);
        
        if ((Facade.initialSnakeHeadPosition.xCoord - Facade.initSnakeTailLength) <= 0)
        {
            Debug.LogError("Init snake tail length too long! was set to zero.");
            return;
        }

        for (int i = 1; i <= Facade.initSnakeTailLength; i++)
        {
            gameField[Facade.initialSnakeHeadPosition.xCoord-i, Facade.initialSnakeHeadPosition.yCoord].SwitchField(FieldElementState.Body);
            gameField[Facade.initialSnakeHeadPosition.xCoord - i, Facade.initialSnakeHeadPosition.yCoord].TTL = Facade.initSnakeTailLength - i;
            gameField[Facade.initialSnakeHeadPosition.xCoord - i, Facade.initialSnakeHeadPosition.yCoord].RecalculateMyColor();
            changedPositions.Push(gameField[Facade.initialSnakeHeadPosition.xCoord - i, Facade.initialSnakeHeadPosition.yCoord]);            
        }     
    }
    public abstract bool ObstacleChecker(FieldElement[,] gameField, ref DecartCoords newHeadPos);

    public virtual FieldElementState CollectableChecker(FieldElement[,] gameField, DecartCoords newHeadPos)
    {
        switch (gameField[newHeadPos.xCoord, newHeadPos.yCoord].MyCurrentState)
        {
            case FieldElementState.Empty:
                goto default;
            case FieldElementState.Body:
                goto default;
            case FieldElementState.Head:
                goto default;
            case FieldElementState.Reward:
                return FieldElementState.Reward;
            case FieldElementState.Wall:
                goto default;
            case FieldElementState.Coloured:
                goto default;
            default:
                return FieldElementState.Empty;
        }
    }   
}