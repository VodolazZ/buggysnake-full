﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeBoxer.Referees
{
    public class GameSettings : MonoBehaviour
    {
        private static GameSettings singletonSetting; //have couple of this object on other scenes

        public int level2Border;
        public int level3Border;
        public int level4Border;
        public int level5Border;
        public int level6Border;
        public int level7Border;
        public int level8Border;
        public int level9Border;
        public int finalBorder = int.MaxValue;
        public int[] borderCollections;
        public Color backgroundColor;

        public Color initialSnakeColor;
        public Color maxFadedSnakeColor;

        public Color Reward1Color;
        public Color WallColor;

        public Color ReloadFieldColor;

        public int fieldWidth;
        public int fieldHeight;

        public int initialSnakeHeadPositionX;
        public int initialSnakeHeadPositionY;

        public int initialSnakeTailLength;

        public string GameMode;
        public GameModes actualGameMode;

        public bool withWalls;
                
        public bool showFieldGrid;
        public float showFieldGridIntensitivity;

        private int wallsQuantity;
        public int WallsQuantity
        {
            get { return wallsQuantity; }
            set
            {
                if (value >= 1 && value < 7) wallsQuantity = value;
                else wallsQuantity = 1;
            }
        }

        public Vector2 snakeHeadIntitialPosition;

        private static DecartCoords snakeHeadInitialPosition;
        public static DecartCoords SnakeHeadInitialPosition
        {
            get { return snakeHeadInitialPosition; }
            private set
            {
                int x = value.xCoord;
                int y = value.yCoord;
                if (x <= 0 || x >= Facade.fieldWidth) x = Facade.fieldWidth / 2;
                if (y <= 0 || y >= Facade.fieldHeight) y = Facade.fieldHeight / 2;
                snakeHeadInitialPosition = new DecartCoords(x, y);
            }
        }

        private void Awake()
        {
            if (singletonSetting == null) singletonSetting = this;
            else if (singletonSetting != this) { Destroy(this); return; }

            DontDestroyOnLoad(this);
            Facade.CurrentGameSettings = this;
            SnakeHeadInitialPosition = new DecartCoords(initialSnakeHeadPositionX, initialSnakeHeadPositionY);
            Facade.SnakeLength = initialSnakeTailLength;

            if (WallsQuantity <= 0 || WallsQuantity > 7) WallsQuantity = 1;
                           
            try
            {
                actualGameMode = (GameModes)Enum.Parse(typeof(GameModes), GameMode);
            }
            catch (Exception)
            {
                Debug.Log("Parsing gametype exception occured! Game mode was set to normal");
                actualGameMode = GameModes.Normal;
            }

            borderCollections = new int[] { 0, level2Border, level3Border, level4Border, level5Border, level6Border, level7Border, level8Border, level9Border, finalBorder };
        }
    }
}