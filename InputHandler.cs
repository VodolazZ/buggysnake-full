﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class InputHandler : MonoBehaviour
{
    public UnityEvent LastGameControlButtonPressed;    

    private static InputHandler currentHandlerLink;

    void Update()
    {
        //direction buttons
        if (Input.GetButtonDown("Up")) Facade.LastChoosenDirection = Directions.Up;
        else if (Input.GetButtonDown("Down")) Facade.LastChoosenDirection = Directions.Down;
        else if (Input.GetButtonDown("Left")) Facade.LastChoosenDirection = Directions.Left;
        else if (Input.GetButtonDown("Right")) Facade.LastChoosenDirection = Directions.Right;


        //func buttons
        if (Input.GetButtonDown("Pause"))
        {
            if (!Facade.isPaused)
                Facade.Pause();
            else Facade.UnPause();
        }
    }
}

public enum Directions 
{
    Up,
    Left,
    Down,    
    Right    
}