﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum GameModes
{
    Normal, LevelReload, NormalWithMirroredBoundaries, GameOver
}

public enum GameModeModifiers
{
    WithWalls
}