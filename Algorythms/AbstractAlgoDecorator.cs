﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class AbstractAlgoDecorator : AbstractLevelAlgorithm
{   
    protected AbstractLevelAlgorithm decoratedAlgo;
    public AbstractLevelAlgorithm DecoratedAlgo
    {
        set { decoratedAlgo = value; }
    }

    public override void GameStepHandler(FieldElement[,] gameField, ref DecartCoords currHeadPos, Stack<FieldElement> changedPositions)
    {
        decoratedAlgo.GameStepHandler(gameField, ref currHeadPos, changedPositions);       
    }
    
    public override bool ObstacleChecker(FieldElement[,] gameField, ref DecartCoords newHeadPos)
    {
        return decoratedAlgo.ObstacleChecker(gameField, ref newHeadPos);
    }

    public override void StartObjectInitializator(FieldElement[,] gameField, Stack<FieldElement> changedPositions)
    {
        decoratedAlgo.StartObjectInitializator(gameField, changedPositions);        
    }

    public override FieldElementState CollectableChecker(FieldElement[,] gameField, DecartCoords newHeadPos)
    {
        return base.CollectableChecker(gameField, newHeadPos);
    }
}
