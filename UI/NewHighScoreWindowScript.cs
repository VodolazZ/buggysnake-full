﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class NewHighScoreWindowScript : MonoBehaviour {

    private const int MAX_PLAYERNAME_LEN = 25;
    private const int MAX_PLAYERAGE = 130;

    private const string BAD_NAME_WARNING = "Incorect player name! Names must be 3-25 symbols";
    private const string BAD_AGE_WARNING = "Incorect age! Please enter whole number between 0 and 130";

    private Record playerRecord;
    private EventSystem sceneEventSys;
    private OnlyKeyBoardInputModule sceneInputModule;

    public Text wrongText;
    public InputField nameField;
    public InputField ageField;

    // Use this for initialization
    void Awake()
    {
        sceneEventSys = FindObjectOfType<EventSystem>();
        sceneInputModule = sceneEventSys.gameObject.GetComponent<OnlyKeyBoardInputModule>();
    }

    void Start()
    {
        nameField.onEndEdit.AddListener(OnNameEntered);
        ageField.onEndEdit.AddListener(OnAgeEntered);
        playerRecord = new Record() { score = Facade.GetPlayerScore(), age = 0, name = "No name" };
        wrongText.gameObject.SetActive(false);          
    }

    void OnEnable()
    {
        SetActiveField(nameField.gameObject);
    }

    private void SetActiveField(GameObject target)
    {
        sceneEventSys.SetSelectedGameObject(null);
        sceneEventSys.SetSelectedGameObject(target);
    }

    public void OnNameEntered(string name)
    {
        Debug.Log(name.Length);
        //IMPLEMENTED BUG неверная бизнес логика: в программе отображено, что длина имени может иметь от трех до 20 символов. Здесь же разрешается ввести 2.
        if (name.Length < 1 || name.Length > MAX_PLAYERNAME_LEN)
        {
            wrongText.text = BAD_NAME_WARNING;
            wrongText.gameObject.SetActive(true);
            SetActiveField(nameField.gameObject);
        }
        else
        {            
            playerRecord.name = name;
            wrongText.gameObject.SetActive(false);
            sceneInputModule.ManuallySwitchToNextField();
        }        
    }

    public void OnAgeEntered(string Age)
    {
        int parsedAge;
        if (int.TryParse(Age, out parsedAge))
        {
            //IMPLEMENTED BUG пропущена проверка на отрицательное число, можно ввести значение от 0 до -maxof int
            if (parsedAge < MAX_PLAYERAGE)
            {
                playerRecord.age = parsedAge;
                //IMPLEMENTED BUG Сообщение об ошибочном вводе не удаляется после ввода корректного значения и перехода к кнопке Submit
                //wrongText.gameObject.SetActive(false);
                sceneInputModule.ManuallySwitchToNextField();
                return;
            }
        }
        wrongText.text = BAD_AGE_WARNING;
        wrongText.gameObject.SetActive(true);
        SetActiveField(ageField.gameObject);
    }

    public void SubmitButtonPressed()
    {
        //Debug.Log("object to be saved. name: " + playerRecord.name + " age: " + playerRecord.age);        
        Facade.AddNewHighscoreRecord(playerRecord);
        Facade.CloseGameScene();
    }
}
