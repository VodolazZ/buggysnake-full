﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Reward1FieldLogic : AbstractFieldLogic
{
    public override Color CalculateMyColor(int TTL)
    {
        return Facade.reward1Color;
    }
}