﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    class WallFieldLogic : AbstractFieldLogic
    {
        public override Color CalculateMyColor(int TTL)
        {
            return Facade.wallColor;
        }
    }
}
