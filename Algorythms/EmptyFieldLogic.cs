﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class EmptyFieldLogic : AbstractFieldLogic
{
    public override Color CalculateMyColor(int TTL)
    {
        if (!Facade.showFieldGrid)
            if (Facade.backgroundColor.a != 0)
                return new Color(Facade.backgroundColor.r, Facade.backgroundColor.g, Facade.backgroundColor.b, 0.0f);
            else return Facade.backgroundColor;
        else return new Color(Facade.backgroundColor.r, Facade.backgroundColor.g, Facade.backgroundColor.b, Facade.showFieldGridIntensitivity);
       
    }
}