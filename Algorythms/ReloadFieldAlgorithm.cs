﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq.Expressions;
using System.Linq;
using System.Collections.Generic;

public class ReloadFieldAlgorithm : AbstractLevelAlgorithm
{
    private static bool fillingComplete = false;
    private static FieldElementState[,] fieldBeforeReload;

    public override void StartObjectInitializator(FieldElement[,] gameField, Stack<FieldElement> changedPositions)
    {        
        fieldBeforeReload = new FieldElementState[Facade.fieldWidth, Facade.fieldHeight];
        for (int i = 0; i < Facade.fieldHeight; i++)
            for (int k = 0; k < Facade.fieldWidth; k++)
            {
                if (gameField[k, i].MyCurrentState != FieldElementState.Head && gameField[k, i].MyCurrentState != FieldElementState.Body)
                    fieldBeforeReload[k, i] = gameField[k, i].MyCurrentState;
                else fieldBeforeReload[k, i] = FieldElementState.Empty;
            }
    }

    public override void GameStepHandler(FieldElement[,] gameField, ref DecartCoords currHeadPos, Stack<FieldElement> changedPositions)
    {
        int colouredRow = 0;
        bool colouringComplete = false;

        if (!fillingComplete)
        {
            for (colouredRow = 0; colouredRow < Facade.fieldHeight; colouredRow++)
            {
                if (gameField[1, colouredRow].MyCurrentState != FieldElementState.Coloured)
                {
                    for (int k = 0; k < Facade.fieldWidth; k++)
                    {
                        gameField[k, colouredRow].SwitchField(FieldElementState.Coloured);
                        gameField[k, colouredRow].TTL = int.MaxValue;
                        changedPositions.Push(gameField[k, colouredRow]);
                    }
                    colouringComplete = true;
                    break;
                }                
            }
        }
        if (!colouringComplete)
        {
            fillingComplete = true;
            for (colouredRow = Facade.fieldHeight - 1; colouredRow >= 0; colouredRow--)
            {
                if (gameField[1, colouredRow].MyCurrentState == FieldElementState.Coloured)
                {
                    for (int k = 0; k < Facade.fieldWidth; k++)
                    {
                        gameField[k, colouredRow].SwitchField(fieldBeforeReload[k, colouredRow]);
                        gameField[k, colouredRow].TTL = int.MaxValue;
                        changedPositions.Push(gameField[k, colouredRow]);
                    }

                    if (Facade.initialSnakeHeadPosition.yCoord - 1 == colouredRow)
                    {
                        base.StartObjectInitializator(gameField, changedPositions);
                        //gameField[Facade.initialSnakeHeadPosition.xCoord, Facade.initialSnakeHeadPosition.yCoord].SwitchField(FieldElementState.Head);
                        //changedPositions.Push(gameField[Facade.initialSnakeHeadPosition.xCoord, Facade.initialSnakeHeadPosition.yCoord]);
                    }
                    colouringComplete = true;
                    break;
                }                
            }
        }

        if (!colouringComplete)
        {
            fillingComplete = false;
            Facade.LevelReloadCompleted();
        }
    }

    public override bool ObstacleChecker(FieldElement[,] gameField, ref DecartCoords newHeadPos)
    {
        return false;
    }
}
