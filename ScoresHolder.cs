﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Reflection;
using scoreboardContainer = System.Collections.Generic.List<Record>;

[Serializable]
public struct Record
{
    public string name;
    public int age;
    public int score;
}

namespace Project.Scores
{
    internal class RecordComparer : IComparer<Record>
    {
        public int Compare(Record x, Record y)
        {

            if (x.score < y.score)
                return 1;
            else if (x.score > y.score)
                return -1;
            else return 0;
        }
    }

    public class ScoresHolder
    {
        public void TryAddNewRecord(Record player)
        {
            Records.Add(player);
            Records.Sort(recComparer);
            //Console.WriteLine("*******AFTERSORTING: ***********");
            //for (int i = 0; i < Records.Count; i++)
            //{
            //    Debug.Log("i: " + i + " " + Records[i].name + " " + Records[i].age + " " + Records[i].score);
            //}

            //IMPLEMENTED BUG строка ниже - ошибка индекса при удалении элемента. После сортировки код должен чистить все элементы выше индекса 6. Фактически каждый раз удаляется элемент 6 (последний рекорд в таблице). В игре это выражено тем, что 7-е место в таблице фактически невозможно занять
            while (Records.Count > 7) Records.RemoveAt(6);
            WriteNewRecordTable(Records);
        }

        public scoreboardContainer GetActualRecordList()
        {
            return Records;
        }

        public bool CheckScoreForRecord(int score)
        {
            if (score > Records[6].score) return true;
            else return false;
        }

        private void WriteNewRecordTable(scoreboardContainer recTable)
        {
            BinaryFormatter Bf = new BinaryFormatter();
            try
            {
                using (FileStream wfs = new FileStream("records.dat", FileMode.Truncate, FileAccess.Write, FileShare.None))
                {
                    Bf.Serialize(wfs, recTable);
                }
            }
            catch (Exception ex)
            {
                Debug.Log("record write problem!");
            }
        }

        private scoreboardContainer Records = new scoreboardContainer();
        private RecordComparer recComparer = new RecordComparer();

        public ScoresHolder()
        {
            BinaryFormatter Bf = new BinaryFormatter();
            try
            {
                if (File.Exists(@"records.dat"))
                {
                    using (Stream stream = File.OpenRead(@"records.dat"))
                    {
                        Records = (scoreboardContainer)Bf.Deserialize(stream);
                        for (int i = 0; i < Records.Count; i++)
                        {                           
                            Debug.Log("i: " + i + " " + Records[i].name + " " + Records[i].age + " " + Records[i].score);
                        }

                    }
                }
                else
                {
                    Records = new scoreboardContainer()
                    {
                        { new Record() {name = "Nad", age = 22, score = 5000 } },
                        { new Record() {name = "Smith", age = 15, score = 4000 } },
                        { new Record() {name = "Lucy", age = 4, score = 3000  } },
                        { new Record() {name = "Nick", age = 15, score = 2000  } },
                        { new Record() {name = "Ollyver", age = 12, score = 1000  } },
                        { new Record() {name = "Pam and her mom", age = 1, score = 500  } },
                        { new Record() {name = "Sally", age = 26, score = 300  } },
                    };

                    using (FileStream writeinfo = new FileStream(@"records.dat", FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        BinaryFormatter bf = new BinaryFormatter();
                        bf.Serialize(writeinfo, Records);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }
    }
}
