﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class GameField : MonoBehaviour
{
    public GameObject FieldElementPrefab;
    private DecartCoords SnakeHeadPosition;    
    private float fieldPrefabOffset;
    private FieldElement[,] gameFieldElements;
    private AbstractLevelAlgorithm currentGameAlgorythm;
    private Stack<FieldElement> ChangedElements;

    private void Awake()
    {        
        ChangedElements = new Stack<FieldElement>();

        ResetGame();        
        fieldPrefabOffset = ((FieldElementPrefab.GetComponentInChildren<SpriteRenderer>()).sprite.rect.height / (FieldElementPrefab.GetComponentInChildren<SpriteRenderer>()).sprite.pixelsPerUnit)*FieldElementPrefab.transform.localScale.y;
        Facade.RegisterForNewStep(HandleStep);
        Facade.RegisterForDefeatConditions(DefeatConditionsHandler);
        Facade.RegisterForLevelReloadComplete(ReloadLevelCompleteHandler);
        Facade.RegisterForGameOverConditionsWereMet(GameOverConditionsWereMetHandler);
        Facade.RegisterForGameOver(GameOverHandler);
    }

    private void ResetGame()
    {
        SnakeHeadPosition = Facade.initialSnakeHeadPosition;
        currentGameAlgorythm = Facade.ActualGameModeAlgo();
    }

    private void Start()
    {
        Initialize();
        Facade.InitializationComplete();
    }

    private void Initialize()
    {
        gameFieldElements = new FieldElement[Facade.fieldWidth, Facade.fieldHeight];
        for (int i = 0; i < Facade.fieldWidth; i++)
            for (int k = 0; k < Facade.fieldHeight; k++)
            {
                gameFieldElements[i, k] = ((GameObject)Instantiate(FieldElementPrefab, new Vector2(i * fieldPrefabOffset, k * fieldPrefabOffset), this.transform.localRotation)).GetComponent<FieldElement>();
                if (Facade.showFieldGrid) gameFieldElements[i, k].Redraw();
            }


        currentGameAlgorythm.StartObjectInitializator(gameFieldElements, ChangedElements);
        RedrawChangedElements();  
    }

    public void HandleStep()
    {
        currentGameAlgorythm.GameStepHandler(gameFieldElements, ref SnakeHeadPosition, ChangedElements);
        RedrawChangedElements();
    }

    private void RedrawChangedElements()
    {
        while (ChangedElements.Count > 0)
        {
            (ChangedElements.Pop()).Redraw();
        }
    }

    public void DefeatConditionsHandler()
    {
        currentGameAlgorythm = Facade.GetSpecialAlgo(GameModes.LevelReload);
        currentGameAlgorythm.StartObjectInitializator(gameFieldElements, ChangedElements);
    }

    public void ReloadLevelCompleteHandler()
    {
        ResetGame();
    }

    public void GameOverConditionsWereMetHandler()
    {
        currentGameAlgorythm = Facade.GetSpecialAlgo(GameModes.GameOver);        
    }

    public void GameOverHandler()
    {
        Facade.UnRegisterForNewStep(HandleStep);
        Facade.UnRegisterForDefeatConditions(DefeatConditionsHandler);
        Facade.UnRegisterForLevelReloadComplete(ReloadLevelCompleteHandler);
        Facade.UnRegisterForGameOverConditionsWereMet(GameOverConditionsWereMetHandler);
        Facade.UnRegisterForGameOver(GameOverHandler);
        Destroy(this);
    }
}