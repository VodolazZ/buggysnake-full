﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Collections;

public class ButtonBehaviour : MonoBehaviour
{
    public void PressedButtonPlay()
    {
        SceneManager.LoadScene("MainGameScene");
    }
    public void PressedButtonSettings()
    {
        SceneManager.LoadScene("SettingsScene");
    }
    public void PressedButtonHighScores()
    {
        SceneManager.LoadScene("HighscoreScene");
    }
}
