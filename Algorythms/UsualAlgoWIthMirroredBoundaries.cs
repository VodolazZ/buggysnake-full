﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class UsualAlgoWIthMirroredBoundaries : UsualGameAlgorithm
{
    public override bool ObstacleChecker(FieldElement[,] gameField, ref DecartCoords newHeadPos)
    {
        if (newHeadPos.xCoord < 0) newHeadPos.xCoord = Facade.fieldWidth - 1;
        else if (newHeadPos.xCoord >= Facade.fieldWidth) newHeadPos.xCoord = 0;
        else if (newHeadPos.yCoord < 0) newHeadPos.yCoord = Facade.fieldHeight - 1;
        else if (newHeadPos.yCoord >= Facade.fieldHeight) newHeadPos.yCoord = 0;

        if (gameField[newHeadPos.xCoord, newHeadPos.yCoord].MyCurrentState == FieldElementState.Body ||
            gameField[newHeadPos.xCoord, newHeadPos.yCoord].MyCurrentState == FieldElementState.Wall ||
            gameField[newHeadPos.xCoord, newHeadPos.yCoord].MyCurrentState == FieldElementState.Head)
            return true;
        else return false;
    }
}


