﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SnakeBodyFieldLogic : AbstractFieldLogic
{
    public override Color CalculateMyColor(int TTL)
    {
        float alpha = Facade.snakeBodyOriginalColor.a - Facade.snakeBodyMaxFadedColor.a;
        alpha = alpha > 0 ? ((alpha / Facade.SnakeLength * TTL) + Facade.snakeBodyMaxFadedColor.a) : Facade.snakeBodyMaxFadedColor.a;
        return new Color(Facade.snakeBodyOriginalColor.r, Facade.snakeBodyOriginalColor.g, Facade.snakeBodyOriginalColor.b, alpha);
    }
}