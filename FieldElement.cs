﻿using UnityEngine;
using System.Collections;

public class FieldElement : MonoBehaviour
{
    public int TTL { get; set; }
    private Color MyColor;
    private FieldElementState myCurrentState;
    public FieldElementState MyCurrentState
    {
        get { return myCurrentState; }
        private set
        {
            myCurrentState = value;
            actualFieldLogic = Facade.GetMyNewBehaviour(MyCurrentState);
        }
    }
    private AbstractFieldLogic actualFieldLogic;
    private SpriteRenderer mySprite;

    private void Awake()
    {
        MyCurrentState = FieldElementState.Empty;
        mySprite = gameObject.GetComponent<SpriteRenderer>();
        TTL = int.MaxValue;
        RecalculateMyColor();     
    }

    public void SwitchField(FieldElementState newState)
    {
        MyCurrentState = newState;
        RecalculateMyColor();        
    }

    public void RecalculateMyColor()
    {
        MyColor = actualFieldLogic.CalculateMyColor(TTL);
    }
     
    public void Redraw()
    {
        actualFieldLogic.Draw(ref mySprite, MyColor);
    }
}
