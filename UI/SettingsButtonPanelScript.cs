﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SettingsButtonPanelScript : MonoBehaviour
{
    public Text modeChanger;
    public Text fieldGrid;
    public Text withWalls;
    public Text wallsQuantity;

    void Start()
    {
        if (Facade.CurrentGameSettings.actualGameMode == GameModes.Normal)
            modeChanger.text = "Game Mode: Normal";
        else
            modeChanger.text = "Game Mode: No Field Boundaries";

        if (!Facade.CurrentGameSettings.showFieldGrid) fieldGrid.text = "Show Field Grid: No";
        else fieldGrid.text = "Show Field Grid: Yes";

        //IMPLEMENTED BUG Пользовательский интерфейс не будет запоминать настройку для стен. Игра будет корректно переключать игровые режимы, но при входе на страницу настроек, всегда будет показано значение "нет".
        //if (!Facade.CurrentGameSettings.withWalls) withWalls.text = "With Walls: No";
        //else withWalls.text = "With Walls: Yes";
    }

    public void ChangeGameMode(Text initiatorText)
    {
        if (Facade.CurrentGameSettings.actualGameMode == GameModes.Normal)
        { Facade.CurrentGameSettings.actualGameMode = GameModes.NormalWithMirroredBoundaries;
            initiatorText.text = "Game Mode: No Field Boundaries";
        }
        else { Facade.CurrentGameSettings.actualGameMode = GameModes.Normal;
            initiatorText.text = "Game Mode: Normal";
        }
    }
    
    public void ShowFieldGridSwitcher(Text initiatorText)
    {
        if (!Facade.CurrentGameSettings.showFieldGrid) { Facade.CurrentGameSettings.showFieldGrid = true; initiatorText.text = "Show Fild Grid: Yes"; }
        else { Facade.CurrentGameSettings.showFieldGrid = false; initiatorText.text = "Show Field Grid: No"; }
    } 

    public void WithWallsSwitcher(Text initiatorText)
    {
        if (!Facade.CurrentGameSettings.withWalls) { Facade.CurrentGameSettings.withWalls = true; initiatorText.text = "With Walls: Yes"; }
        else { Facade.CurrentGameSettings.withWalls = false; initiatorText.text = "With Walls: No"; }
    }

    public void WallsQuantitySwitcher(Text initiatorText)
    {
        Facade.CurrentGameSettings.WallsQuantity++;
        initiatorText.text = "Walls Qontity: " + Facade.CurrentGameSettings.WallsQuantity;
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("TitleScreenScene");       
    }
}
