﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeBoxer.UI
{
    class UISpeedScript : MonoBehaviour
    {
        private const string baseText = "Speed: ";
        private Text scoreText;

        void Awake()
        {
            scoreText = GetComponent<Text>();
        }
        void Start()
        {
            Facade.RegisterForChangeLevel(LevelChangeHandler);
            Facade.RegisterForGameOver(GameOverHandler);
            LevelChangeHandler(1);
        }

        public void LevelChangeHandler(int newScoreValue)
        {
            //IMPLEMENTED BUG всего уровней 9, но как только уровень поднимается выше 7, отображается этот уровень как 6 и больше не повышается.
            if (newScoreValue > 7 || newScoreValue < 0) newScoreValue = 6; 
            scoreText.text = baseText + newScoreValue;
        }

        public void GameOverHandler()
        {
            Facade.UnRegisterForChangeLevel(LevelChangeHandler);
            Facade.UnRegisterForGameOver(GameOverHandler);
            Destroy(this);
        }
    }
}
