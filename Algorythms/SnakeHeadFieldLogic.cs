﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public sealed class SnakeHeadFieldLogic : AbstractFieldLogic
{
    public override Color CalculateMyColor(int TTL)
    {
        return Facade.snakeBodyOriginalColor;
    }
}