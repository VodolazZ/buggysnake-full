﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;


class OnlyKeyBoardInputModule : StandaloneInputModule
{
    //initial value
    private bool isMouseInputActive = false;

    //type interface to get actual mouse input status
    public bool GetMouseState
    {
        get { return isMouseInputActive; }
    }

    //mouse switcher interface 
    public void MouseSwitcher()
    {
        isMouseInputActive = isMouseInputActive == false ? true : false;
    }

    public void ManuallySwitchToNextField()
    {
        ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, GetAxisEventData(0, -1, 0.6f), ExecuteEvents.moveHandler);        
    }

    //inherited event processing interface (called every frame)
    public override void Process()
    {
        bool usedEvent = SendUpdateEventToSelectedObject();

        if (eventSystem.sendNavigationEvents)
        {
            if (!usedEvent)
                usedEvent |= SendMoveEventToSelectedObject();

            if (!usedEvent)
                SendSubmitEventToSelectedObject();
        }

        if (isMouseInputActive)
            ProcessMouseEvent();
    }
}