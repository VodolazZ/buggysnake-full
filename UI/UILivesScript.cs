﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeBoxer.UI
{
    class UILivesScript : MonoBehaviour
    {
        private const string baseText = "Lives: ";
        private const char LIVE_PICT = '@';
        private Text livesText;

        void Awake()
        {
            livesText = GetComponent<Text>();
        }
        void Start()
        {
            livesText.text = baseText;
            Facade.RegisterForDefeatConditions(LivesChangeHandler);
            Facade.RegisterForGameOver(GameOverHandler);
            LivesChangeHandler();
        }

        public void LivesChangeHandler()
        {
            StringBuilder newlivesText = new StringBuilder(baseText);
            for (uint i = 0; i < Facade.NumberOfLives; i++)
            {
                newlivesText.Append(LIVE_PICT);
            }
            livesText.text = newlivesText.ToString();
        }

        public void GameOverHandler()
        {
            Facade.UnRegisterForDefeatConditions(LivesChangeHandler);
            Facade.UnRegisterForGameOver(GameOverHandler);
            Destroy(this);
        }
    }
}
