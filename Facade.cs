﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using SnakeBoxer.Referees;
using System.Linq;
using System.Text;

public struct DecartCoords
{
    public int xCoord;
    public int yCoord;

    public DecartCoords(int x, int y) { xCoord = x; yCoord = y; }
}

public static class Facade
{
    //common vars
    public static int SnakeLength;
    public static byte NumberOfLives
    {
        get { return currentGameReferee.CurrentNumOfLives; }
        set { currentGameReferee.CurrentNumOfLives = value; }
    }
    //***

    //Game settings facade
    private static GameSettings currentGameSettings;
    public static GameSettings CurrentGameSettings
    {
        set { currentGameSettings = value; }
        get { return currentGameSettings; }
    }
    public static Color backgroundColor {  get { return currentGameSettings.backgroundColor; } }
    public static Color snakeBodyOriginalColor { get { return currentGameSettings.initialSnakeColor; } }
    public static Color snakeBodyMaxFadedColor { get { return currentGameSettings.maxFadedSnakeColor; } }
    public static Color reward1Color { get { return currentGameSettings.Reward1Color; } }
    public static Color wallColor { get { return currentGameSettings.WallColor; } }
    public static Color reloadFieldColor {  get { return currentGameSettings.ReloadFieldColor; } }

    public static DecartCoords initialSnakeHeadPosition { get { return GameSettings.SnakeHeadInitialPosition; } }
    public static int initSnakeTailLength { get { return currentGameSettings.initialSnakeTailLength; } }

    public static int fieldWidth { get { return currentGameSettings.fieldWidth; } }
    public static int fieldHeight { get { return currentGameSettings.fieldHeight; } }

    public static bool showFieldGrid { get { return CurrentGameSettings.showFieldGrid; } }
    public static float showFieldGridIntensitivity { get { return CurrentGameSettings.showFieldGridIntensitivity; } }
    public static int numberOfWallsToBePlaced { get { return CurrentGameSettings.WallsQuantity * 5; } }

    //***

    //Game referee facade
    private static GameProcessReferee currentGameReferee;
    public static GameProcessReferee CurrentGameReferee
    {        
        set { currentGameReferee = value; }
    }

    public static bool isPaused
    {
        get { return currentGameReferee.isPaused; }
    }

    public static void Pause()
    {
        currentGameReferee.TurnOnPausePanel();
    }

    public static void UnPause()
    {
        currentGameReferee.TurnOffPausePanel();
    }

    public static int GetPlayerScore()
    {
        return (int)currentGameReferee.Score;
    }

    public static List<Record> GetScoreBoard()
    {
        return currentGameReferee.myScoreBoard.GetActualRecordList();
    }

    public static void CloseGameScene()
    {
        currentGameReferee.CloseGameScene();
    }

    public static bool CheckForRecord(int score)
    {
        return currentGameReferee.myScoreBoard.CheckScoreForRecord(score);
    }

    public static void AddNewHighscoreRecord(Record player)
    {
        currentGameReferee.myScoreBoard.TryAddNewRecord(player);
    }

    public static void InitializationComplete()
    {
        currentGameReferee.InitializationComplete();
    }

    public static int CurrentLevel { get { return currentGameReferee.CurrentLevel; } }

    public static void RegisterForNewStep(Action newHandler)
    {
        GameProcessReferee.nextStepHasBeenMade += newHandler;
    }
    public static void RegisterForScoreChange(Action<uint> newHandler)
    {
        GameProcessReferee.ChangeScoreEvent += newHandler;
    }
    public static void RegisterForDefeatConditions(Action newHandler)
    {
        GameProcessReferee.DefeatConditions += newHandler;
    }
    public static void RegisterForLevelReloadComplete(Action newHandler)
    {
        GameProcessReferee.LevelReloadComplete += newHandler;
    }

    public static void RegisterForGameOverConditionsWereMet(Action newHadler)
    {
        GameProcessReferee.GameOverConditionsWereMet += newHadler;
    }

    public static void RegisterForGameOver(Action newHandler)
    {
        GameProcessReferee.GameOverReloadCompleted += newHandler;
    }

    public static void RegisterForChangeLevel(Action<int> newHandler)
    {
        GameProcessReferee.ChangeLevelEvent += newHandler;
    }

    public static void UnRegisterForChangeLevel(Action<int> newHandler)
    {
        GameProcessReferee.ChangeLevelEvent -= newHandler;
    }

    public static void UnRegisterForNewStep(Action newHandler)
    {
        GameProcessReferee.nextStepHasBeenMade -= newHandler;
    }
    public static void UnRegisterForScoreChange(Action<uint> newHandler)
    {
        GameProcessReferee.ChangeScoreEvent -= newHandler;
    }
    public static void UnRegisterForDefeatConditions(Action newHandler)
    {
        GameProcessReferee.DefeatConditions -= newHandler;
    }
    public static void UnRegisterForLevelReloadComplete(Action newHandler)
    {
        GameProcessReferee.LevelReloadComplete -= newHandler;
    }
    public static void UnRegisterForGameOver(Action newHandler)
    {
        GameProcessReferee.GameOverReloadCompleted -= newHandler;
    }
    public static void UnRegisterForGameOverConditionsWereMet(Action newHadler)
    {
        GameProcessReferee.GameOverConditionsWereMet -= newHadler;
    }

    public static void GameOverConditionsWereMet()
    {
        currentGameReferee.InitiateGameOverConditionsWereMet();
    }

    public static void GameOverFulfilled()
    {
        currentGameReferee.GameOverReloadScreenFinished();
    }
        
    public static void InitiateDeath()
    {
        currentGameReferee.DefeatConditionsFulfilled();
    }

    public static void LevelReloadCompleted()
    {
        currentGameReferee.LevelReloadCompleted();
    }

    public static void ScoreChanged(int n)
    {
        currentGameReferee.ScoreChanged(n);
    }

    public static Directions lastStepDirection { get { return currentGameReferee.lastStepDirection; } set { currentGameReferee.lastStepDirection = value; } }
    public static Directions LastChoosenDirection
    {
        get { return currentGameReferee.lastChoosenDirection; }
        set
        {
            if (!CheckOpposites(currentGameReferee.lastStepDirection, value))
                currentGameReferee.lastChoosenDirection = value;
        }
    }
    

    private static bool CheckOpposites(Directions prev, Directions next)
    {
        int baseInt = 2;
        if (((int)prev % baseInt) == ((int)next % baseInt)) return true;
        else return false;
    }    
        
    //***
    
    #region AlgorythmsCollections
    
    private static GameModes CurrentMode { get { return currentGameSettings.actualGameMode; } }
    private static Dictionary<GameModes, AbstractLevelAlgorithm> GameModeAlgorithms = new Dictionary<GameModes, AbstractLevelAlgorithm>()
    {
        {GameModes.Normal, new UsualGameAlgorithm() },
        {GameModes.NormalWithMirroredBoundaries, new UsualAlgoWIthMirroredBoundaries() },
        {GameModes.LevelReload, new ReloadFieldAlgorithm() },
        {GameModes.GameOver, new GameOverReloadFieldAlgorithm() }
    };

    private static Dictionary<GameModeModifiers, AbstractLevelAlgorithm> GameModeAlgoModifiers = new Dictionary<GameModeModifiers, AbstractLevelAlgorithm>()
    {
        {
            GameModeModifiers.WithWalls, new WallAlgoDecorator()
        }
    };
            
    public static AbstractLevelAlgorithm ActualGameModeAlgo()
    {
        AbstractLevelAlgorithm levAlgo;
        GameModeAlgorithms.TryGetValue(CurrentMode, out levAlgo);
        if (currentGameSettings.withWalls)
        {
            AbstractLevelAlgorithm wrappedAlgo = levAlgo;
            GameModeAlgoModifiers.TryGetValue(GameModeModifiers.WithWalls, out levAlgo);
            (levAlgo as WallAlgoDecorator).DecoratedAlgo = wrappedAlgo;
        }
        return levAlgo;
    }

    public static AbstractLevelAlgorithm GetSpecialAlgo(GameModes neededAlgo)
    {
        AbstractLevelAlgorithm algo;
        GameModeAlgorithms.TryGetValue(neededAlgo, out algo);
        return algo;
    }

    private static Dictionary<FieldElementState, AbstractFieldLogic> LogicClasses = new Dictionary<FieldElementState, AbstractFieldLogic>()
    {
        { FieldElementState.Head, new SnakeHeadFieldLogic() },
        { FieldElementState.Body, new SnakeBodyFieldLogic() },
        { FieldElementState.Empty, new EmptyFieldLogic() },
        { FieldElementState.Reward, new Reward1FieldLogic() },
        { FieldElementState.Wall, new WallFieldLogic() },
        { FieldElementState.Coloured, new ColouredFieldLogic() },
    };

    public static AbstractFieldLogic GetMyNewBehaviour(FieldElementState newState)
    {
        AbstractFieldLogic answer;
        LogicClasses.TryGetValue(newState, out answer);
        return answer;
    }

    #endregion
}