﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public class HighScoresScreenScript : MonoBehaviour
{
    public Text firstPlaceName;
    public Text firstPlaceScore;

    public Text secondPlaceName;
    public Text secondPlaceScore;

    public Text thirdPlaceName;
    public Text thirdPlaceScore;

    public Text forthPlaceName;
    public Text forthPlaceScore;

    public Text fifthPlaceName;
    public Text fifthPlaceScore;

    public Text sixthPlaceName;
    public Text sixthPlaceScore;

    public Text seventhPlaceName;
    public Text seventhPlaceScore;

    List<Record> records;

    void Awake()
    {
        records = Facade.GetScoreBoard();
        //for (int i = 0; i < records.Count; i++)
        //{
        //    Debug.Log("i: " + i + " " + records[i].name + " " + records[i].age + " " + records[i].score);            
        //}        
    }

    void Start()
    {
        firstPlaceName.text = string.Format(records[0].name + " (age: " + records[0].age + ")");
        firstPlaceScore.text = (records[0].score).ToString();

        secondPlaceName.text = string.Format(records[1].name + " (age: " + records[1].age + ")");
        secondPlaceScore.text = (records[1].score).ToString();

        thirdPlaceName.text = string.Format(records[2].name + " (age: " + records[2].age + ")");
        thirdPlaceScore.text = (records[2].score).ToString();

        forthPlaceName.text = string.Format(records[3].name + " (age: " + records[3].age + ")");
        forthPlaceScore.text = (records[3].score).ToString();

        fifthPlaceName.text = string.Format(records[4].name + " (age: " + records[4].age + ")");
        fifthPlaceScore.text = (records[4].score).ToString();

        sixthPlaceName.text = string.Format(records[5].name + " (age: " + records[5].age + ")");
        sixthPlaceScore.text = (records[5].score).ToString();

        seventhPlaceName.text = string.Format(records[6].name + " (age: " + records[6].age + ")");
        seventhPlaceScore.text = (records[6].score).ToString();
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("TitleScreenScene");
    }
}
